import React from 'react'
import { format, distanceInWords, differenceInDays } from 'date-fns'

import { buildImageObj } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

import BlockContent from './block-content'

const ParagraphClass = 'dib relative w-100 dark-gray fw4 mb4'
const HeadingClass = 'dib relative w-100 measure-wide f2 b ma0 brand-navy'

function BlogPost(props) {
  const { _rawBody, categories, title, mainImage, publishedAt } = props

  return (
    <article>
      <section className="dib relative w-100 lh-body dark-gray f5 post">
        <div className="db center mw7 ph4 pv6">
          {categories && categories.length > 0 ? (
            <small className="dib relative w-100 ttu f7 tracked b">{categories[0].title}</small>
          ) : null}
          <h1 className={HeadingClass}>{title || 'Blog post'}</h1>
          {publishedAt && (
            <p className={`${ParagraphClass} f7 o-60`}>
              Posted:{' '}
              {differenceInDays(new Date(publishedAt), new Date()) > 3
                ? distanceInWords(new Date(publishedAt), new Date())
                : format(new Date(publishedAt), 'MMMM Do YYYY')}
            </p>
          )}
          {mainImage && mainImage.asset && (
            <img
              className="dib relative w-100 fit mv2 mh-40"
              src={imageUrlFor(buildImageObj(mainImage)).url()}
              alt={mainImage.alt}
            />
          )}
          <div className={ParagraphClass}>
            {_rawBody ? <BlockContent blocks={_rawBody} /> : null}
          </div>
        </div>
      </section>
    </article>
  )
}

export default BlogPost
