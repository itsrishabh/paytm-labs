import React from 'react'
import Modal from 'react-modal'
import ReactGA from 'react-ga'

class JobOpeningItem extends React.Component {
  constructor(...props) {
    super(...props)

    this.state = {
      modalIsOpen: false
    }

    this.afterOpenModal = this.afterOpenModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.openModal = this.openModal.bind(this)
    this.logClick = this.logClick.bind(this)
  }

  openModal() {
    this.setState({
      modalIsOpen: true
    })
  }

  afterOpenModal() {
    ReactGA.event({
      category: 'Jobs',
      action: 'Viewed Job',
      label: this.props.job.text
    })
  }

  closeModal() {
    this.setState({
      modalIsOpen: false
    })
  }

  logClick() {
    ReactGA.event({
      category: 'Jobs',
      action: 'Clicked job link',
      label: this.props.job.text
    })
  }

  render() {
    const { job } = this.props

    return (
      <div className="dib relative w-100 pv3 mb2 role">
        {/* Modal */}
        <Modal
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          isOpen={this.state.modalIsOpen}
          contentLabel={`Viewing ${job.text}`}
          overlayClassName="modal-overlay"
          className="modal"
        >
          <div className="dib relative w-100 tl lh-copy">
            <button
              className="absolute dt center h2 w2 pa1 bg-near-white br3 shadow-light black pointer close"
              onClick={this.closeModal}
            >
              <img className="dtc v-mid w-100 h-100" src="/icons/close.svg" alt="Close" />
            </button>
            <div className="dib relative w-100">
              <strong className="dib relative w-100 f4 b dark-gray mb3">{job.text}</strong>
              {job.description ? (
                <div
                  className="dib relative w-100 dark-gray f6 lever"
                  dangerouslySetInnerHTML={{ __html: job.description }}
                />
              ) : null}
              <p className="dib relative w-100 gray f7">
                {job.categories.commitment || `Full-time`} • {job.categories.location}
              </p>
              <a
                className="dib relative w-100 bg-brand-blue ph3 pv3 br3 f5 b white tc link dim mt4 z-1"
                onClick={this.logClick}
                target="_blank"
                href={job.applyUrl}
                rel="noopener noreferrer"
              >
                Apply for this role
              </a>
            </div>
          </div>
        </Modal>

        {/* Job */}
        <div className="dib relative w-100">
          <div className="flex middle-xs relative w-100 lh-copy tl">
            <div className="dib relative v-mid w-100">
              <strong className="dib relative w-100 f5 b dark-gray">{job.text}</strong>
              <p className="gray f7">
                {job.categories.commitment || `Full-time`} • {job.categories.location}
              </p>
            </div>

            <div className="dib relative v-mid tr">
              <a
                className="dib relative v-mid w-auto tc f6 b brand-blue bg-transparent ba bw1 b--brand-blue tc ph3 pv2 br3 link dim pointer"
                onClick={this.logClick}
                target="_blank"
                href={job.hostedUrl}
                rel="noopener noreferrer"
              >
                View
              </a>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Modal.setAppElement('body')

export default JobOpeningItem
