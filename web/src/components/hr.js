import React from 'react'

class HR extends React.Component {
  render() {
    const { fullWidth, thick } = this.props

    return (
      <hr
        className={`dib relative bb b--navy mv4 w-100 ${fullWidth ? 'w-100' : 'mw3'} ${
          thick ? 'bw3' : 'bw2'
        }`}
      />
    )
  }
}

export default HR
