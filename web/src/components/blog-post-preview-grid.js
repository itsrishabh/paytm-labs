import { Link } from 'gatsby'
import React from 'react'

import BlogPostPreview from './blog-post-preview'

const HeadingClass = 'dib relative w-100 measure-wide f2 b mv3 black'

function BlogPostPreviewGrid(props) {
  return (
    <div className="dib relative w-100">
      {props.title && (
        <h2 className={HeadingClass}>
          {props.browseMoreHref ? (
            <Link to={props.browseMoreHref}>{props.title}</Link>
          ) : (
            props.title
          )}
        </h2>
      )}
      {props.nodes && props.nodes.length > 0 ? (
        <div className="row">
          {props.nodes.map(node => (
            <div className="col-xs-12 col-md-4" key={node.id}>
              <BlogPostPreview {...node} />
            </div>
          ))}
        </div>
      ) : null}
      {props.browseMoreHref && (
        <div>
          <Link to={props.browseMoreHref}>Browse more</Link>
        </div>
      )}
    </div>
  )
}

BlogPostPreviewGrid.defaultProps = {
  title: '',
  nodes: [],
  browseMoreHref: ''
}

export default BlogPostPreviewGrid
