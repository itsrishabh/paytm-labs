import React from 'react'

const Container = ({ children }) => {
  return <div className="db center mw8 ph4 pv6">{children}</div>
}

export default Container
