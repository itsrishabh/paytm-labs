import React from 'react'
import { Link } from 'gatsby'

import { buildImageObj } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

function ProjectPreviewGrid(props) {
  return (
    <div className="row items-top">
      {props.nodes &&
        props.nodes.map((object, index) => (
          <div
            className={props.nodes.length <= 2 ? 'col-xs-12 col-md-6' : 'col-xs-12 col-md-4'}
            key={`prod-${index}`}
          >
            <Link
              className="dib relative w-100 bg-white link dim black related mb4 mb0-l ba b--black-10"
              to={`/products/${object.slug.current}`}
            >
              <div
                className="dib relative w-100 relative image"
                style={{
                  backgroundRepeat: 'repeat',
                  backgroundImage: `url(${imageUrlFor(buildImageObj(object.mainImage))})`,
                  backgroundSize: 'contain'
                }}
              >
                <div className="absolute left-0 top-0 w2 h2 pa1 bg-white z-1 br-pill overflow-hidden ma4 shadow-4">
                  <img
                    className="dib relative w-100 h-100 fit"
                    alt={object.title}
                    src={imageUrlFor(buildImageObj(object.icon))}
                  />
                </div>
                {/* <img
                  className="dib relative w-100 h-100 fit"
                  alt={object.title}
                  src={imageUrlFor(buildImageObj(object.mainImage))}
                /> */}
              </div>
              <div className="dib relative w-100 pa3">
                <strong className="dib relative w-100 f5 b ma0">{object.title}</strong>
                <p className="dib relative w-100 ma0 f7 gray">{object.description}</p>
              </div>
            </Link>
          </div>
        ))}
    </div>
  )
}

ProjectPreviewGrid.defaultProps = {
  browseMoreHref: '',
  title: '',
  nodes: []
}

export default ProjectPreviewGrid
