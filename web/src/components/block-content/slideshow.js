import React, { useState } from 'react'

import { buildImageObj } from '../../lib/helpers'
import { imageUrlFor } from '../../lib/image-url'

function Slideshow(props) {
  if (!props.slides) return null

  const len = props.slides.length

  // eslint-disable-next-line
  const [index, setIndex] = useState(0)

  function handlePrev() {
    setIndex(Math.max(index - 1, 0))
  }

  function handleNext() {
    setIndex(Math.min(index + 1, len - 1))
  }

  return (
    <div className="dib relative w-100">
      <div className="w-100 absolute z-1">
        <button onClick={handlePrev} disabled={index === 0}>
          Prev
        </button>
        <span>
          {index + 1} of {len}
        </span>
        <button onClick={handleNext} disabled={index === len - 1}>
          Next
        </button>
      </div>
      <ul
        className="carousel"
        data-index={index}
        style={{ transform: `translate3d(${index * -100}%, 0, 0)` }}
      >
        {props.slides.map(slide => (
          <li key={slide._key} className="slide">
            {slide.asset && (
              <img
                alt=""
                src={imageUrlFor(buildImageObj(slide))
                  .width(1200)
                  .height(Math.floor((9 / 16) * 1200))
                  .fit('crop')
                  .url()}
              />
            )}
          </li>
        ))}
      </ul>
    </div>
  )
}

export default Slideshow
