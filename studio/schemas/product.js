export default {
  name: 'product',
  title: 'Product',
  type: 'document',
  fields: [
    {
      name: 'icon',
      title: 'Product icon',
      type: 'image'
    },
    {
      name: 'title',
      title: 'Title',
      type: 'string'
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      description:
        'Create a slug based on the title or other means to be picked up by both SEO and serve as a mini landing page for each product',
      options: {
        source: 'title',
        maxLength: 96
      }
    },
    {
      name: 'publishedAt',
      title: 'Published at',
      description: 'You can add a launch date (optional)',
      type: 'datetime'
    },
    {
      name: 'excerpt',
      title: 'Excerpt',
      type: 'text'
    },
    {
      name: 'mainImage',
      title: 'Main image',
      type: 'mainImage'
    },
    {
      name: 'title2',
      title: 'Title 2',
      type: 'string'
    },
    {
      name: 'body',
      title: 'Body',
      type: 'blockContent'
    },
    {
      name: 'listTitle',
      title: 'Stats title',
      type: 'string'
    },
    {
      name: 'listItems',
      title: 'Stats items',
      of: [
        {
          type: 'listItems'
        }
      ],
      type: 'array'
    },
    {
      name: 'secondImage',
      title: 'Secondary image',
      type: 'mainImage'
    },
    {
      name: 'valueTitle',
      title: 'Value props title',
      type: 'string'
    },
    {
      name: 'valueItems',
      title: 'Value props',
      of: [
        {
          type: 'listItems'
        }
      ],
      type: 'array'
    },
    {
      name: 'relatedProducts',
      title: 'Related products',
      type: 'array',
      of: [{ type: 'reference', to: { type: 'product' } }]
    }
  ],
  preview: {
    select: {
      title: 'title',
      publishedAt: 'publishedAt',
      image: 'mainImage'
    },
    prepare({ title = 'No title', publishedAt, image }) {
      return {
        title,
        subtitle: publishedAt
          ? new Date(publishedAt).toLocaleDateString()
          : 'Missing publishing date',
        media: image
      }
    }
  }
}
