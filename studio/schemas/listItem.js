export default {
  name: 'listItems',
  type: 'object',
  title: 'List items',
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title',
      description: 'Heading of the list item'
    },
    {
      name: 'description',
      type: 'text',
      title: 'Description',
      description: 'Description of the list item'
    },
    {
      name: 'icon',
      type: 'figure',
      title: 'Icon'
    },
    {
      name: 'link',
      type: 'url',
      title: 'Link (optional)',
      description: 'If you add a link the list item the frontend will add a link'
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'description',
      media: 'icon'
    }
  }
}
