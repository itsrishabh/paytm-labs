import { MdInsertPhoto } from 'react-icons/md'

export default {
  name: 'partner',
  title: 'Partner',
  type: 'document',
  icon: MdInsertPhoto,
  liveEdit: false,
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string'
    },
    {
      name: 'image',
      title: 'Image',
      type: 'image',
      options: {
        hotspot: true
      }
    }
  ],
  preview: {
    select: {
      title: 'name',
      media: 'image'
    }
  }
}
