export default {
  __experimental_actions: ['update', 'publish', 'create'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'About image',
      type: 'figure'
    },
    {
      name: 'body',
      title: 'About body',
      type: 'blockContent'
    }
  ],
  title: 'About Page',
  type: 'document',
  name: 'aboutPage'
}
