export default {
  __experimental_actions: ['update', 'publish'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'Stats image',
      type: 'figure'
    },
    {
      name: 'body',
      title: 'Stats body',
      type: 'blockContent'
    }
  ],
  title: 'Stats Page',
  type: 'document',
  name: 'statsPage'
}
