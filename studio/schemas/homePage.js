export default {
  __experimental_actions: ['update', 'publish'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'banner',
      title: 'Show banner on home page',
      type: 'boolean'
    },
    {
      name: 'bannerText',
      title: 'Banner text',
      description: 'Title of banner shown on the home page if the toggle is enabled',
      type: 'string'
    },
    {
      name: 'bannerDescription',
      title: 'Banner description',
      description: 'Description of banner shown on the home page if the toggle is enabled',
      type: 'string'
    },
    {
      name: 'bannetCTA',
      title: 'Banner cta',
      description: 'CTA text on banner',
      type: 'string'
    },
    {
      name: 'bannerCTALink',
      title: 'Banner link',
      description: 'Where should this banner go?',
      type: 'string'
    },
    {
      name: 'bannerCTAExternal',
      title: 'Banner link external?',
      description: 'If the banner link is external please enable this toggle',
      type: 'boolean'
    },
    {
      name: 'image',
      title: 'Home image',
      type: 'figure'
    },
    {
      name: 'body',
      title: 'Home body',
      type: 'blockContent'
    },
    {
      name: 'story',
      title: 'Story title',
      type: 'string'
    },
    {
      name: 'storyBody',
      title: 'Story body',
      type: 'text'
    },
    {
      name: 'values',
      title: 'Values title',
      type: 'string'
    },
    {
      name: 'valuesBody',
      title: 'Values body',
      type: 'text'
    },
    {
      name: 'valueLI',
      title: 'Values items',
      of: [
        {
          type: 'listItems'
        }
      ],
      type: 'array'
    },
    {
      name: 'equality',
      title: 'Equality, diversity title',
      type: 'string'
    },
    {
      name: 'equalityBody',
      title: 'Equality, diversity body',
      type: 'text'
    },
    {
      name: 'equalityImage',
      title: 'Equality, diversity image',
      type: 'figure'
    },
    {
      name: 'benefits',
      title: 'Benefits title',
      type: 'string'
    },
    {
      name: 'showBlue',
      title: 'Show light blue text',
      type: 'boolean'
    },
    {
      name: 'benefitsBlue',
      title: 'Light blue text',
      type: 'string'
    },
    {
      name: 'benefitsLI',
      title: 'Benefit items',
      of: [
        {
          type: 'listItems'
        }
      ],
      type: 'array'
    },
    {
      name: 'benefitsImage',
      title: 'Benefits image',
      type: 'figure'
    },
    {
      name: 'careers',
      title: 'Careers title',
      type: 'string'
    },
    {
      name: 'careersBody',
      title: 'Careers body',
      type: 'text'
    },
    {
      name: 'showGPTW',
      title: 'Show great place to work',
      type: 'boolean'
    },
    {
      name: 'gptwLink',
      title: 'Great place to work link',
      type: 'url'
    },
    {
      name: 'gptwBody',
      title: 'Great place to work body',
      type: 'string'
    },
    {
      name: 'careersCTA',
      title: 'See all careers CTA',
      type: 'string'
    },
    {
      name: 'careersLink',
      title: 'Careers link',
      type: 'url'
    },
    {
      name: 'careersThanks',
      title: 'Careers bottom body',
      type: 'text'
    }
  ],
  title: 'Home Page',
  type: 'document',
  name: 'homePage'
}
