export default {
  __experimental_actions: ['update', 'publish'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'Insights image',
      type: 'figure'
    },
    {
      name: 'body',
      title: 'Insights body',
      type: 'blockContent'
    }
  ],
  title: 'Insights Page',
  type: 'document',
  name: 'insightPage'
}
